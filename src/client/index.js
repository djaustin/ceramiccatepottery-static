import React from 'react';
import { render } from 'react-dom';
import {
  HashRouter,
  Route,
  IndexLink,
  Switch,
  hashHistory,
} from 'react-router-dom';

import '../css/vendor/bootstrap.min.css';
import '../css/vendor/bootstrap-social.min.css';
import '../css/vendor/font-awesome.min.css';
import '../css/vollkorn-font.css';
import '../css/main.css';

import LandingPage from './components/LandingPage.jsx';
import Galleries from './components/Galleries.jsx';
import Gallery from './components/Gallery.jsx';
import Contact from './components/Contact.jsx';

const App = () =>
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route exact path="/galleries" component={Galleries} />
    <Route path="/galleries/:slug" component={Gallery} />
    <Route exact path="/contact" component={Contact} />
  </Switch>;

render(
  <HashRouter>
    <App />
  </HashRouter>,
  document.getElementById('app')
);
