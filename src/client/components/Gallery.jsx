import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Lightbox from 'react-images';

import '../../css/main.css';
import * as constants from './constants';
import Header from './Header.jsx';
import Footer from './Footer.jsx';

import LoadingIndicator from './LoadingIndicator.jsx';

const GalleryHeader = ({ galleryName }) =>
  <div>
    <Header />
    <h1 id="header">
      <Link to={`/galleries`}>&raquo;Galleries</Link>
      &raquo;{galleryName}
    </h1>
  </div>;

const GalleryHeaderWithLoader = ({ galleryName }) =>
  <div>
    <GalleryHeader galleryName={galleryName} />
    <LoadingIndicator />
    <Footer />
  </div>;

export default class Gallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      galleryName: '',
      gallerySlug: '',
      images: [],
      lightboxIsOpen: false,
      currentImage: 0,
    };

    this.slug = props.match.params.slug;

    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    this.gotoImage = this.gotoImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
  }

  openLightbox(index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  gotoImage(index) {
    this.setState({
      currentImage: index,
    });
  }
  handleClickImage() {
    if (this.state.currentImage === this.props.images.length - 1) return;

    this.gotoNext();
  }

  componentDidMount() {
    axios.get(`${constants.api_url}/galleries/${this.slug}`).then(res => {
      const gallery = res.data;
      this.setState({
        galleryName: gallery.name,
        gallerySlug: gallery.slug,
        images: gallery.originalImages.map(function(image) {
          var img = {};
          img['id'] = `${image.id}`;
          img['src'] = `${constants.s3_url}/images/uploads/${image.digest}`;
          img['caption'] = `${image.description}`;
          img['name'] = `${image.name}`;
          img[
            'thumbnail'
          ] = `${constants.s3_url}/images/uploads/${image.digest}_thumb`;
          return img;
        }),
      });
    });
  }

  render() {
    return this.state.images.length === 0
      ? <GalleryHeaderWithLoader galleryName={this.state.galleryName} />
      : <div>
          <GalleryHeader galleryName={this.state.galleryName} />
          <div className="container content">
            <div className="row">
              <div className="col-md-offset-2 col-md-8">
                <div className="row">
                  {this.state.images.map((obj, i) =>
                    <div key={`${i}`} className="thumbnail col-sm-4">
                      <a
                        href={`${obj.src}`}
                        onClick={e => this.openLightbox(i, e)}
                      >
                        <img src={`${obj.thumbnail}`} />
                      </a>
                      <div className="caption text-center">
                        {`${obj.name}`}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <Lightbox
            images={this.state.images}
            isOpen={this.state.lightboxIsOpen}
            onClickImage={this.handleClickImage}
            onClickPrev={this.gotoPrevious}
            onClickThumbnail={this.gotoImage}
            currentImage={this.state.currentImage}
            onClickNext={this.gotoNext}
            onClose={this.closeLightbox}
            showThumbnails={true}
            backdropClosesModal={true}
          />
          <Footer />
        </div>;
  }
}
