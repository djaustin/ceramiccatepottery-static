import React from 'react';
import Header from './Header.jsx';

import Footer from './Footer.jsx';

export default class Contact extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container-fluid">
          <h1>Contact Cate</h1>

          <form
            method="POST"
            enctype="application/x-www-form-urlencoded"
            action="/contact"
            id="contact-form"
            data-toggle="validator"
            role="form"
            className="form-horizontal"
          >
            <div className="row">
              <div className="col-sm-6 col-lg-4">
                <div className="form-group">
                  <label for="contact-name" className="col-md-4 control-label">
                    Your name
                  </label>
                  <div className="col-md-8">
                    <input
                      type="text"
                      className="form-control"
                      id="contact-name"
                      name="name"
                      data-error="Your name is required"
                      required
                    />
                  </div>
                  <div className="help-block with-errors" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-6 col-lg-4">
                <div className="form-group">
                  <label for="contact-email" className="col-md-4 control-label">
                    Your email (optional, but required if you want me to get in
                    touch with you)
                  </label>
                  <div className="col-md-8">
                    <input
                      type="text"
                      className="form-control"
                      id="contact-email"
                      name="email"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-6 col-lg-4">
                <div className="form-group">
                  <label
                    for="contact-comments"
                    className="col-md-4 control-label"
                  >
                    Your comments:
                  </label>
                  <div className="col-md-8">
                    <textarea
                      className="form-control"
                      id="contact-comments"
                      name="comments"
                      rows="5"
                      cols="30"
                      data-error="Your comments are required"
                      required
                    />
                  </div>
                  <div className="help-block with-errors" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-6 col-lg-4">
                <div className="form-group">
                  <div className="col-md-8">
                    <button type="submit" className="btn btn-primary">
                      Send
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <Footer />
      </div>
    );
  }
}
