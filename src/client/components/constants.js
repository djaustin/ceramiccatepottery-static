const api = function() {
  let api_url = '';
  if (process.env.APP_ENV == 'local') {
    api_url = 'http://192.168.1.3:8080/api';
  } else if (
    process.env.APP_ENV == 'development' ||
    process.env.APP_ENV == 'staging'
  ) {
    api_url = 'https://ceramiccatepottery-staging.herokuapp.com/api';
  } else if (process.env.APP_ENV == 'prod') {
    api_url = 'https://ceramiccatepottery-production.herokuapp.com/api';
  }
  console.log('**** API URL ' + api_url);
  return api_url;
};

const s3 = function() {
  let s3_url = '';
  if (process.env.APP_ENV == 'local') {
    s3_url = 'https://s3.amazonaws.com/dev.ceramiccatepottery.com';
  } else if (process.env.APP_ENV == 'development') {
    s3_url = 'https://dev.ceramiccatepottery.com';
  } else if (process.env.APP_ENV == 'staging') {
    s3_url = 'https://staging.ceramiccatepottery.com';
  } else if (process.env.APP_ENV == 'prod') {
    s3_url = 'https://static.ceramiccatepottery.com';
  }
  return s3_url;
};

export const api_url = api();
export const s3_url = s3();
