import React from 'react';
import { Link } from 'react-router-dom';

import * as constants from './constants';

import '../../css/main.css';
import '../../css/landing-page.css';
import Footer from './Footer.jsx';

export default class LandingPage extends React.Component {
  render() {
    return (
      <div className="background-image">
        <div className="container-fluid">
          <div className="row">
            <h3 className="text-center front-page-content">Cate Austin</h3>
          </div>
          <div className="row">
            <h1 className="text-center front-page-content">Studio Potter</h1>
          </div>
          <div className="row">
            <div className="text-center view-work-btn">
              <Link to="/galleries">Gallery</Link>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
