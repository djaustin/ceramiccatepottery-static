import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Header from './Header.jsx';
import Footer from './Footer.jsx';

import LoadingIndicator from './LoadingIndicator.jsx';

import '../../css/main.css';
import * as constants from './constants';

// move to another component
const GalleryHeader = () =>
  <div>
    <Header />
    <Link to="/galleries">
      <h1 id="header">&raquo;Galleries</h1>
    </Link>
  </div>;
// move to another component

const GalleryHeaderWithLoader = () =>
  <div>
    <GalleryHeader />
    <LoadingIndicator />
    <Footer />
  </div>;
// move to another component
const Images = ({ galleries }) =>
  <div>
    <GalleryHeader />
    <div className="container content">
      <div className="row">
        <div className="col-md-offset-2 col-md-8">
          <div className="row">
            {galleries.map(gallery =>
              <div key={gallery.id} className="thumbnail col-sm-4">
                <Link to={`/galleries/${gallery.slug}`}>
                  <img
                    src={`${constants.s3_url}/images/uploads/${gallery.coverPhoto}_thumb`}
                    className="img-responsive"
                    alt={`${gallery.description}`}
                  />
                </Link>

                <div className="caption text-center">
                  {gallery.name}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
    <Footer />
  </div>;

export default class Gallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      galleries: [],
    };
  }

  componentDidMount() {
    axios.get(`${constants.api_url}/galleries`).then(res => {
      this.setState({ galleries: res.data });
    });
  }

  render() {
    return this.state.galleries.length === 0
      ? <GalleryHeaderWithLoader />
      : <Images galleries={this.state.galleries} />;
  }
}
