import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="navbar navbar-default navbar-fixed-bottom">
        <div className="container-fluid">
          <div className="navbar-header">
            <p className="navbar-text">
              Copyright &copy; 2017 CeramicCatePottery.com
            </p>
          </div>

          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <p className="navbar-text ">
                  <a
                    href="https://www.facebook.com/ceramicatebrus"
                    className="btn btn-social-icon btn-facebook"
                  >
                    <span className="fa fa-facebook" />
                  </a>
                </p>
              </li>
              <li>
                <p className="navbar-text">
                  <a
                    href="https://www.instagram.com/ceramicateaustin/"
                    className="btn btn-social-icon btn-instagram"
                  >
                    <span className="fa fa-instagram" />
                  </a>
                </p>
              </li>
              <li>
                <p className="navbar-text">
                  <Link
                    to="/contact"
                    className="btn btn-social-icon btn-envelope"
                  >
                    <i className="fa fa-envelope fa-2x" />
                  </Link>
                </p>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    );
  }
}
