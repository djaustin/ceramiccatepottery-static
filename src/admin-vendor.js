// css
require('./css/admin/metisMenu.css');
require('./css/admin/timeline.css');
require('./css/admin/admin.css');
require('./css/admin/morris.css');

// js
require('./js/admin/metisMenu.js');
require('./js/admin/raphael.js');
require('./js/admin/morris.js');
require('./js/admin/morris-data.js');
require('./js/admin/admin.js');
