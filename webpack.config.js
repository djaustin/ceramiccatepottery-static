var path = require("path");
var PROD = JSON.stringify(process.env.NODE_ENV) === "production" ? true : false
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
var CriticalPlugin = require('webpack-plugin-critical').CriticalPlugin;


var jsOutputFileName;
var baseAssetUrl = ".ceramiccatepottery.com";
var localAssetUrl = "https://s3.amazonaws.com/dev" + baseAssetUrl;
var devAssetUrl = "https://dev" + baseAssetUrl;
var stagingAssetUrl = "https://staging" + baseAssetUrl;
var envAssetUrl;
console.log("***** NODE ENV  " + process.env.NODE_ENV);
console.log("***** APP ENV  " + process.env.APP_ENV);

if(process.env.NODE_ENV === "production") {
  jsOutputFileName = '[name].[chunkhash].js';
} else {
  jsOutputFileName = '[name].js'
}
// clean this up
if (process.env.APP_ENV === "local") {
  envAssetUrl = localAssetUrl;
}

if (process.env.APP_ENV === "development") {
  envAssetUrl = devAssetUrl;
}

if (process.env.APP_ENV === "staging") {
  envAssetUrl = stagingAssetUrl;
}

var cssLoaders = [{
  loader: 'css-loader',
  options: {
    name: "/css/[name]-[contenthash:8].[ext]"
  }
}]
const webpack = require('webpack');
module.exports = {
  entry: {
    index: './src/client/index.js', 
  },
  output: {
    path: path.resolve(__dirname, './assets'),
    filename:  jsOutputFileName,
    publicPath: '/'
  },
  resolve: {
    modules: [
      path.join(__dirname, "src"),
      "node_modules",
      "web_modules",
      "bower_components"
    ],
  },
  module: {
    rules: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.jsx$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        use: cssLoaders
      })
    }, {
      test: /\.(eot|svg|woff|woff2|ttf)$/,
      use: [{
        loader: 'url-loader',
        options: {
          name: 'fonts/[name].[ext]',
          limit: 10000
        }
      }]
    }, {
      test: /.*\.(gif|png|jpe?g|svg)$/i,
      use: [{
        loader: 'file-loader',
        options: {
          hash: 'sha512',
          digest: 'hex',
          name: 'images/[hash].[ext]',
          limit: 10000
        }
      }, {
        loader: 'image-webpack-loader'
      }]
    }, ]
  },
  devServer: {
    port: 9000
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'css/[name].css',
      allChunks: true,
    }),
    new HtmlWebpackPlugin({
      title: "CeramicCate Pottery",
      assetUrl: envAssetUrl,
      template: "./src/client/index.html"
    }),
    new ScriptExtHtmlWebpackPlugin({
    defaultAttribute: 'async'
    }),
    new CriticalPlugin({
    src: 'index.html',
    inline: true,
    minify: true,
    dest: 'index.html'
  }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'APP_ENV': JSON.stringify(process.env.APP_ENV)
      }
    }),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({names: ['jquery-2.1.4', 'bootstrap.min'], minChunks: Infinity, filename: 'vendor.js'})
  ]
}